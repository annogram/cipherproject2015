﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CipherProject {
    static class Program {
        /// <summary>
        /// This program is a simple text encoder. The vision is to create multiple different cyphers which can be chosen from that can be encoded and decoded
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Cipher());
        }
    }
}

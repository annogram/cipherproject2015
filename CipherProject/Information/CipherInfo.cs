﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CipherProject.Information {
    public class CipherInfo {
        /// <summary>
        /// Takes the tostring method of a cipher and returns the alphabet thats used.
        /// </summary>
        /// <param name="FunctionName">The Ciphers ToString() return</param>
        /// <returns>CipherAlphabet The characters that can be used with this cipher</returns>
        public string GetInfo(string FunctionName) {
            string prefix = "This cipher uses: ";
            switch (FunctionName) {
                case "Morse code":
                    return prefix + "abcdefghijklmnopqrstuvwxyz1234567890 ";
                default:
                    return prefix + Util.Constants.DEFAULT_ALPHA;
            }
        }

        
        /// <summary>
        /// Returns a help message depending on certian events in the program
        /// </summary>
        /// <param name="command">An enumerated list of parameters which can be processed</param>
        /// <returns>Relavent information on an operation</returns>
        public string GetInfo(CipherInfoData command) {
            switch (command) {
                case CipherInfoData.Initial:
                default:
                    return "Type a message to begin.";
                case CipherInfoData.Working:
                    return "Working...";
                case CipherInfoData.Done:
                    return "Done.";
                case CipherInfoData.KeyWordSet:
                    return "Keyword set";
                case CipherInfoData.ShiftSet:
                    return "Alphabet shifted";
                case CipherInfoData.Clipboard:
                    return "Copied encoded text to clipboard";
                case CipherInfoData.Clear:
                    return "All text cleared";
                case CipherInfoData.OpenTextFile:
                    return "Text added to editor";
                case CipherInfoData.SaveTextFile:
                    return "File saved";
                case CipherInfoData.BadFormat:
                    return "This text is not correctly formatted for this fucntion";
            }
        }
    }

    public enum CipherInfoData{
        Initial,
        Working,
        Done,
        KeyWordSet,
        ShiftSet,
        Clipboard,
        Clear,
        OpenTextFile,
        SaveTextFile,
        BadFormat
    }
}

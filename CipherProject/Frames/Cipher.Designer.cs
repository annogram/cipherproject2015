﻿namespace CipherProject {
    partial class Cipher {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cipher));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openTextFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveTextFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createNewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.infoPanel = new System.Windows.Forms.Panel();
            this.infoLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.CryptographicText = new System.Windows.Forms.TextBox();
            this.InputArea = new System.Windows.Forms.TextBox();
            this.TextEditorHeader = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.SelectCipher = new System.Windows.Forms.ListBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.playMorseCode = new System.Windows.Forms.Button();
            this.keywordSet = new System.Windows.Forms.Button();
            this.setShiftButton = new System.Windows.Forms.Button();
            this.shiftMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.keyWordText = new System.Windows.Forms.TextBox();
            this.shiftLabel = new System.Windows.Forms.Label();
            this.keywordLabel = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.clipboard = new System.Windows.Forms.Button();
            this.ClearText = new System.Windows.Forms.Button();
            this.cryptWorker = new System.ComponentModel.BackgroundWorker();
            this.openTextFile = new System.Windows.Forms.OpenFileDialog();
            this.saveTextFile = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.infoPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Black;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.customToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(956, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openTextFileToolStripMenuItem,
            this.saveTextFileToolStripMenuItem});
            this.fileToolStripMenuItem.ForeColor = System.Drawing.Color.Lime;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(50, 29);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openTextFileToolStripMenuItem
            // 
            this.openTextFileToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.openTextFileToolStripMenuItem.ForeColor = System.Drawing.Color.Lime;
            this.openTextFileToolStripMenuItem.Name = "openTextFileToolStripMenuItem";
            this.openTextFileToolStripMenuItem.Size = new System.Drawing.Size(211, 30);
            this.openTextFileToolStripMenuItem.Text = "Open text file..";
            this.openTextFileToolStripMenuItem.Click += new System.EventHandler(this.openTextFileToolStripMenuItem_Click);
            // 
            // saveTextFileToolStripMenuItem
            // 
            this.saveTextFileToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.saveTextFileToolStripMenuItem.ForeColor = System.Drawing.Color.Lime;
            this.saveTextFileToolStripMenuItem.Name = "saveTextFileToolStripMenuItem";
            this.saveTextFileToolStripMenuItem.Size = new System.Drawing.Size(211, 30);
            this.saveTextFileToolStripMenuItem.Text = "Save text file...";
            this.saveTextFileToolStripMenuItem.Click += new System.EventHandler(this.saveTextFileToolStripMenuItem_Click);
            // 
            // customToolStripMenuItem
            // 
            this.customToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createNewToolStripMenuItem});
            this.customToolStripMenuItem.ForeColor = System.Drawing.Color.Lime;
            this.customToolStripMenuItem.Name = "customToolStripMenuItem";
            this.customToolStripMenuItem.Size = new System.Drawing.Size(86, 29);
            this.customToolStripMenuItem.Text = "Custom";
            // 
            // createNewToolStripMenuItem
            // 
            this.createNewToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.createNewToolStripMenuItem.ForeColor = System.Drawing.Color.Lime;
            this.createNewToolStripMenuItem.Name = "createNewToolStripMenuItem";
            this.createNewToolStripMenuItem.Size = new System.Drawing.Size(206, 30);
            this.createNewToolStripMenuItem.Text = "One time pad";
            this.createNewToolStripMenuItem.Click += new System.EventHandler(this.createNewToolStripMenuItem_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.21339F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.78661F));
            this.tableLayoutPanel1.Controls.Add(this.infoPanel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.button1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 33);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 78.3237F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.6763F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(956, 398);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // infoPanel
            // 
            this.infoPanel.AutoSize = true;
            this.infoPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.infoPanel, 2);
            this.infoPanel.Controls.Add(this.infoLabel);
            this.infoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infoPanel.Location = new System.Drawing.Point(3, 366);
            this.infoPanel.Name = "infoPanel";
            this.infoPanel.Size = new System.Drawing.Size(950, 29);
            this.infoPanel.TabIndex = 6;
            // 
            // infoLabel
            // 
            this.infoLabel.AutoSize = true;
            this.infoLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.infoLabel.ForeColor = System.Drawing.Color.Lime;
            this.infoLabel.Location = new System.Drawing.Point(0, 0);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(186, 20);
            this.infoLabel.TabIndex = 0;
            this.infoLabel.Text = "Type a message to begin";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Maroon;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(635, 288);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(318, 72);
            this.button1.TabIndex = 3;
            this.button1.Text = "Encode";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.CryptographicText);
            this.panel1.Controls.Add(this.InputArea);
            this.panel1.Controls.Add(this.TextEditorHeader);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(626, 279);
            this.panel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Lime;
            this.label2.Location = new System.Drawing.Point(307, -1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Output";
            // 
            // CryptographicText
            // 
            this.CryptographicText.BackColor = System.Drawing.Color.Aquamarine;
            this.CryptographicText.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.CryptographicText.Dock = System.Windows.Forms.DockStyle.Left;
            this.CryptographicText.Location = new System.Drawing.Point(307, 20);
            this.CryptographicText.Multiline = true;
            this.CryptographicText.Name = "CryptographicText";
            this.CryptographicText.ReadOnly = true;
            this.CryptographicText.Size = new System.Drawing.Size(316, 259);
            this.CryptographicText.TabIndex = 2;
            // 
            // InputArea
            // 
            this.InputArea.BackColor = System.Drawing.Color.Honeydew;
            this.InputArea.Dock = System.Windows.Forms.DockStyle.Left;
            this.InputArea.Location = new System.Drawing.Point(0, 20);
            this.InputArea.Multiline = true;
            this.InputArea.Name = "InputArea";
            this.InputArea.Size = new System.Drawing.Size(307, 259);
            this.InputArea.TabIndex = 1;
            this.InputArea.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.InputArea_KeyPress);
            // 
            // TextEditorHeader
            // 
            this.TextEditorHeader.AutoSize = true;
            this.TextEditorHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.TextEditorHeader.ForeColor = System.Drawing.Color.Lime;
            this.TextEditorHeader.Location = new System.Drawing.Point(0, 0);
            this.TextEditorHeader.Name = "TextEditorHeader";
            this.TextEditorHeader.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TextEditorHeader.Size = new System.Drawing.Size(145, 20);
            this.TextEditorHeader.TabIndex = 0;
            this.TextEditorHeader.Text = "Text to be encoded";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(635, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(318, 279);
            this.panel2.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.SelectCipher);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 42);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(318, 237);
            this.panel4.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(25, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(211, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Select an encryption method";
            // 
            // SelectCipher
            // 
            this.SelectCipher.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SelectCipher.BackColor = System.Drawing.Color.Black;
            this.SelectCipher.ForeColor = System.Drawing.Color.Lime;
            this.SelectCipher.FormattingEnabled = true;
            this.SelectCipher.ItemHeight = 20;
            this.SelectCipher.Location = new System.Drawing.Point(0, 30);
            this.SelectCipher.Name = "SelectCipher";
            this.SelectCipher.ScrollAlwaysVisible = true;
            this.SelectCipher.Size = new System.Drawing.Size(318, 104);
            this.SelectCipher.TabIndex = 4;
            this.SelectCipher.SelectedIndexChanged += new System.EventHandler(this.SelectCipher_SelectedIndexChanged);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.playMorseCode);
            this.panel6.Controls.Add(this.keywordSet);
            this.panel6.Controls.Add(this.setShiftButton);
            this.panel6.Controls.Add(this.shiftMaskedTextBox);
            this.panel6.Controls.Add(this.keyWordText);
            this.panel6.Controls.Add(this.shiftLabel);
            this.panel6.Controls.Add(this.keywordLabel);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 155);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(318, 82);
            this.panel6.TabIndex = 5;
            // 
            // playMorseCode
            // 
            this.playMorseCode.BackColor = System.Drawing.Color.Black;
            this.playMorseCode.ForeColor = System.Drawing.Color.Lime;
            this.playMorseCode.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.playMorseCode.Location = new System.Drawing.Point(14, 0);
            this.playMorseCode.Name = "playMorseCode";
            this.playMorseCode.Size = new System.Drawing.Size(75, 32);
            this.playMorseCode.TabIndex = 6;
            this.playMorseCode.Text = "Play";
            this.playMorseCode.UseVisualStyleBackColor = false;
            this.playMorseCode.Visible = false;
            this.playMorseCode.Click += new System.EventHandler(this.button2_Click);
            // 
            // keywordSet
            // 
            this.keywordSet.BackColor = System.Drawing.Color.Black;
            this.keywordSet.ForeColor = System.Drawing.Color.Lime;
            this.keywordSet.Location = new System.Drawing.Point(253, 42);
            this.keywordSet.Name = "keywordSet";
            this.keywordSet.Size = new System.Drawing.Size(54, 30);
            this.keywordSet.TabIndex = 5;
            this.keywordSet.Text = "set";
            this.keywordSet.UseVisualStyleBackColor = false;
            this.keywordSet.Visible = false;
            this.keywordSet.Click += new System.EventHandler(this.keywordSet_Click);
            // 
            // setShiftButton
            // 
            this.setShiftButton.BackColor = System.Drawing.Color.Black;
            this.setShiftButton.ForeColor = System.Drawing.Color.Lime;
            this.setShiftButton.Location = new System.Drawing.Point(104, 3);
            this.setShiftButton.Name = "setShiftButton";
            this.setShiftButton.Size = new System.Drawing.Size(59, 33);
            this.setShiftButton.TabIndex = 2;
            this.setShiftButton.Text = "set";
            this.setShiftButton.UseVisualStyleBackColor = false;
            this.setShiftButton.Visible = false;
            this.setShiftButton.Click += new System.EventHandler(this.setShiftButton_Click);
            // 
            // shiftMaskedTextBox
            // 
            this.shiftMaskedTextBox.BackColor = System.Drawing.Color.Honeydew;
            this.shiftMaskedTextBox.BeepOnError = true;
            this.shiftMaskedTextBox.Location = new System.Drawing.Point(61, 3);
            this.shiftMaskedTextBox.Mask = "#00";
            this.shiftMaskedTextBox.Name = "shiftMaskedTextBox";
            this.shiftMaskedTextBox.Size = new System.Drawing.Size(36, 26);
            this.shiftMaskedTextBox.TabIndex = 1;
            this.shiftMaskedTextBox.Visible = false;
            // 
            // keyWordText
            // 
            this.keyWordText.BackColor = System.Drawing.Color.Honeydew;
            this.keyWordText.Location = new System.Drawing.Point(88, 42);
            this.keyWordText.Name = "keyWordText";
            this.keyWordText.Size = new System.Drawing.Size(159, 26);
            this.keyWordText.TabIndex = 4;
            this.keyWordText.Visible = false;
            // 
            // shiftLabel
            // 
            this.shiftLabel.AutoSize = true;
            this.shiftLabel.ForeColor = System.Drawing.Color.Lime;
            this.shiftLabel.Location = new System.Drawing.Point(12, 6);
            this.shiftLabel.Name = "shiftLabel";
            this.shiftLabel.Size = new System.Drawing.Size(42, 20);
            this.shiftLabel.TabIndex = 0;
            this.shiftLabel.Text = "Shift";
            this.shiftLabel.Visible = false;
            // 
            // keywordLabel
            // 
            this.keywordLabel.AutoSize = true;
            this.keywordLabel.ForeColor = System.Drawing.Color.Lime;
            this.keywordLabel.Location = new System.Drawing.Point(13, 42);
            this.keywordLabel.Name = "keywordLabel";
            this.keywordLabel.Size = new System.Drawing.Size(69, 20);
            this.keywordLabel.TabIndex = 3;
            this.keywordLabel.Text = "Keyword";
            this.keywordLabel.Visible = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.radioButton1);
            this.panel3.Controls.Add(this.radioButton2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(318, 42);
            this.panel3.TabIndex = 4;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButton1.ForeColor = System.Drawing.Color.Lime;
            this.radioButton1.Location = new System.Drawing.Point(0, 0);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(89, 42);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Encode";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged_1);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Dock = System.Windows.Forms.DockStyle.Right;
            this.radioButton2.ForeColor = System.Drawing.Color.Lime;
            this.radioButton2.Location = new System.Drawing.Point(228, 0);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(90, 42);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Decode";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged_1);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.clipboard);
            this.panel5.Controls.Add(this.ClearText);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 288);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(626, 72);
            this.panel5.TabIndex = 5;
            // 
            // clipboard
            // 
            this.clipboard.BackColor = System.Drawing.Color.Black;
            this.clipboard.Dock = System.Windows.Forms.DockStyle.Right;
            this.clipboard.ForeColor = System.Drawing.Color.Lime;
            this.clipboard.Location = new System.Drawing.Point(423, 0);
            this.clipboard.Name = "clipboard";
            this.clipboard.Size = new System.Drawing.Size(203, 72);
            this.clipboard.TabIndex = 5;
            this.clipboard.Text = "Copy to clipboard";
            this.clipboard.UseVisualStyleBackColor = false;
            this.clipboard.Click += new System.EventHandler(this.clipboard_Click_1);
            // 
            // ClearText
            // 
            this.ClearText.BackColor = System.Drawing.Color.Black;
            this.ClearText.Dock = System.Windows.Forms.DockStyle.Left;
            this.ClearText.FlatAppearance.BorderColor = System.Drawing.Color.Salmon;
            this.ClearText.FlatAppearance.BorderSize = 3;
            this.ClearText.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.ClearText.ForeColor = System.Drawing.Color.Lime;
            this.ClearText.Location = new System.Drawing.Point(0, 0);
            this.ClearText.Name = "ClearText";
            this.ClearText.Size = new System.Drawing.Size(181, 72);
            this.ClearText.TabIndex = 4;
            this.ClearText.Text = "Clear text";
            this.ClearText.UseVisualStyleBackColor = false;
            this.ClearText.Click += new System.EventHandler(this.ClearText_Click);
            // 
            // cryptWorker
            // 
            this.cryptWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.cryptWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.cryptographicWorker_RunWorkerCompleted);
            // 
            // openTextFile
            // 
            this.openTextFile.FileName = "textfile.txt";
            this.openTextFile.Filter = "Text files| *.txt|All files|*.*";
            this.openTextFile.InitialDirectory = "Environment.GetEnvironmentVariable(\"USERPROFILE\");";
            this.openTextFile.RestoreDirectory = true;
            this.openTextFile.Title = "Open text file";
            this.openTextFile.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // saveTextFile
            // 
            this.saveTextFile.FileName = "save text.txt";
            this.saveTextFile.Filter = "Text files|*.txt|All files|*.*";
            // 
            // Cipher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(956, 431);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(980, 900);
            this.MinimumSize = new System.Drawing.Size(978, 456);
            this.Name = "Cipher";
            this.Text = "Akrams Cryptogram";
            this.Load += new System.EventHandler(this.Cipher_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.infoPanel.ResumeLayout(false);
            this.infoPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openTextFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveTextFileToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label TextEditorHeader;
        private System.ComponentModel.BackgroundWorker cryptWorker;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.ToolStripMenuItem customToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createNewToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox CryptographicText;
        private System.Windows.Forms.TextBox InputArea;
        private System.Windows.Forms.Button ClearText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox SelectCipher;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.MaskedTextBox shiftMaskedTextBox;
        private System.Windows.Forms.Label shiftLabel;
        private System.Windows.Forms.Button keywordSet;
        private System.Windows.Forms.TextBox keyWordText;
        private System.Windows.Forms.Label keywordLabel;
        private System.Windows.Forms.Button setShiftButton;
        private System.Windows.Forms.Button clipboard;
        private System.Windows.Forms.OpenFileDialog openTextFile;
        private System.Windows.Forms.SaveFileDialog saveTextFile;
        private System.Windows.Forms.Panel infoPanel;
        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.Button playMorseCode;
    }
}


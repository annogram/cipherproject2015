﻿namespace CipherProject.Frames {
    partial class OneTimePad {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OneTimePad));
            this.MainLayout = new System.Windows.Forms.TableLayoutPanel();
            this.generatorPanel = new System.Windows.Forms.Panel();
            this.copyKey = new System.Windows.Forms.Button();
            this.genRandom = new System.Windows.Forms.Button();
            this.genTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.outputTextBox = new System.Windows.Forms.TextBox();
            this.keyTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.keyLabel = new System.Windows.Forms.Label();
            this.messageLabel = new System.Windows.Forms.Label();
            this.messageTextBox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.copyMessage = new System.Windows.Forms.Button();
            this.decoideRadio = new System.Windows.Forms.RadioButton();
            this.encodeRadio = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.copyCipher = new System.Windows.Forms.Button();
            this.decodeButton = new System.Windows.Forms.Button();
            this.MainLayout.SuspendLayout();
            this.generatorPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainLayout
            // 
            this.MainLayout.BackColor = System.Drawing.Color.Black;
            this.MainLayout.ColumnCount = 3;
            this.MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.MainLayout.Controls.Add(this.generatorPanel, 0, 2);
            this.MainLayout.Controls.Add(this.outputTextBox, 2, 1);
            this.MainLayout.Controls.Add(this.keyTextBox, 1, 1);
            this.MainLayout.Controls.Add(this.label2, 2, 0);
            this.MainLayout.Controls.Add(this.keyLabel, 1, 0);
            this.MainLayout.Controls.Add(this.messageLabel, 0, 0);
            this.MainLayout.Controls.Add(this.messageTextBox, 0, 1);
            this.MainLayout.Controls.Add(this.panel1, 0, 2);
            this.MainLayout.Controls.Add(this.panel2, 2, 2);
            this.MainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainLayout.Location = new System.Drawing.Point(0, 0);
            this.MainLayout.Name = "MainLayout";
            this.MainLayout.RowCount = 3;
            this.MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.39726F));
            this.MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 92.60274F));
            this.MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.MainLayout.Size = new System.Drawing.Size(802, 411);
            this.MainLayout.TabIndex = 0;
            // 
            // generatorPanel
            // 
            this.generatorPanel.Controls.Add(this.copyKey);
            this.generatorPanel.Controls.Add(this.genRandom);
            this.generatorPanel.Controls.Add(this.genTextBox);
            this.generatorPanel.Controls.Add(this.label3);
            this.generatorPanel.Controls.Add(this.label1);
            this.generatorPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.generatorPanel.Location = new System.Drawing.Point(270, 307);
            this.generatorPanel.Name = "generatorPanel";
            this.generatorPanel.Size = new System.Drawing.Size(261, 101);
            this.generatorPanel.TabIndex = 10;
            // 
            // copyKey
            // 
            this.copyKey.BackColor = System.Drawing.Color.Black;
            this.copyKey.Cursor = System.Windows.Forms.Cursors.Hand;
            this.copyKey.Dock = System.Windows.Forms.DockStyle.Top;
            this.copyKey.ForeColor = System.Drawing.Color.Lime;
            this.copyKey.Location = new System.Drawing.Point(0, 20);
            this.copyKey.Name = "copyKey";
            this.copyKey.Size = new System.Drawing.Size(261, 43);
            this.copyKey.TabIndex = 10;
            this.copyKey.Text = "Copy Text to Clipboard";
            this.copyKey.UseVisualStyleBackColor = false;
            this.copyKey.Click += new System.EventHandler(this.copyKey_Click);
            // 
            // genRandom
            // 
            this.genRandom.BackColor = System.Drawing.Color.Black;
            this.genRandom.Cursor = System.Windows.Forms.Cursors.Hand;
            this.genRandom.ForeColor = System.Drawing.Color.Lime;
            this.genRandom.Location = new System.Drawing.Point(169, 60);
            this.genRandom.Name = "genRandom";
            this.genRandom.Size = new System.Drawing.Size(92, 34);
            this.genRandom.TabIndex = 5;
            this.genRandom.Text = "Generate";
            this.genRandom.UseVisualStyleBackColor = false;
            this.genRandom.Click += new System.EventHandler(this.genRandom_Click);
            // 
            // genTextBox
            // 
            this.genTextBox.Location = new System.Drawing.Point(65, 64);
            this.genTextBox.Mask = "000000000000";
            this.genTextBox.Name = "genTextBox";
            this.genTextBox.Size = new System.Drawing.Size(100, 26);
            this.genTextBox.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Lime;
            this.label3.Location = new System.Drawing.Point(0, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "Length";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Generate random key";
            // 
            // outputTextBox
            // 
            this.outputTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.outputTextBox.Location = new System.Drawing.Point(537, 25);
            this.outputTextBox.Multiline = true;
            this.outputTextBox.Name = "outputTextBox";
            this.outputTextBox.ReadOnly = true;
            this.outputTextBox.Size = new System.Drawing.Size(262, 276);
            this.outputTextBox.TabIndex = 5;
            // 
            // keyTextBox
            // 
            this.keyTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.keyTextBox.Location = new System.Drawing.Point(270, 25);
            this.keyTextBox.Multiline = true;
            this.keyTextBox.Name = "keyTextBox";
            this.keyTextBox.Size = new System.Drawing.Size(261, 276);
            this.keyTextBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.ForeColor = System.Drawing.Color.Lime;
            this.label2.Location = new System.Drawing.Point(537, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(262, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Output";
            // 
            // keyLabel
            // 
            this.keyLabel.AutoSize = true;
            this.keyLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.keyLabel.ForeColor = System.Drawing.Color.Lime;
            this.keyLabel.Location = new System.Drawing.Point(270, 0);
            this.keyLabel.Name = "keyLabel";
            this.keyLabel.Size = new System.Drawing.Size(261, 20);
            this.keyLabel.TabIndex = 1;
            this.keyLabel.Text = "Key";
            // 
            // messageLabel
            // 
            this.messageLabel.AutoSize = true;
            this.messageLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.messageLabel.ForeColor = System.Drawing.Color.Lime;
            this.messageLabel.Location = new System.Drawing.Point(3, 0);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(261, 20);
            this.messageLabel.TabIndex = 0;
            this.messageLabel.Text = "Message";
            // 
            // messageTextBox
            // 
            this.messageTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.messageTextBox.Location = new System.Drawing.Point(3, 25);
            this.messageTextBox.Multiline = true;
            this.messageTextBox.Name = "messageTextBox";
            this.messageTextBox.Size = new System.Drawing.Size(261, 276);
            this.messageTextBox.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.copyMessage);
            this.panel1.Controls.Add(this.decoideRadio);
            this.panel1.Controls.Add(this.encodeRadio);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 307);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(261, 101);
            this.panel1.TabIndex = 7;
            // 
            // copyMessage
            // 
            this.copyMessage.BackColor = System.Drawing.Color.Black;
            this.copyMessage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.copyMessage.Dock = System.Windows.Forms.DockStyle.Top;
            this.copyMessage.ForeColor = System.Drawing.Color.Lime;
            this.copyMessage.Location = new System.Drawing.Point(0, 0);
            this.copyMessage.Name = "copyMessage";
            this.copyMessage.Size = new System.Drawing.Size(261, 43);
            this.copyMessage.TabIndex = 9;
            this.copyMessage.Text = "Copy Message";
            this.copyMessage.UseVisualStyleBackColor = false;
            this.copyMessage.Click += new System.EventHandler(this.copyMessage_Click);
            // 
            // decoideRadio
            // 
            this.decoideRadio.AutoSize = true;
            this.decoideRadio.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.decoideRadio.ForeColor = System.Drawing.Color.Lime;
            this.decoideRadio.Location = new System.Drawing.Point(0, 53);
            this.decoideRadio.Name = "decoideRadio";
            this.decoideRadio.Size = new System.Drawing.Size(261, 24);
            this.decoideRadio.TabIndex = 1;
            this.decoideRadio.TabStop = true;
            this.decoideRadio.Text = "Decode";
            this.decoideRadio.UseVisualStyleBackColor = true;
            this.decoideRadio.CheckedChanged += new System.EventHandler(this.decoideRadio_CheckedChanged);
            // 
            // encodeRadio
            // 
            this.encodeRadio.AutoSize = true;
            this.encodeRadio.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.encodeRadio.ForeColor = System.Drawing.Color.Lime;
            this.encodeRadio.Location = new System.Drawing.Point(0, 77);
            this.encodeRadio.Name = "encodeRadio";
            this.encodeRadio.Size = new System.Drawing.Size(261, 24);
            this.encodeRadio.TabIndex = 0;
            this.encodeRadio.TabStop = true;
            this.encodeRadio.Text = "Encode";
            this.encodeRadio.UseVisualStyleBackColor = true;
            this.encodeRadio.CheckedChanged += new System.EventHandler(this.encodeRadio_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.copyCipher);
            this.panel2.Controls.Add(this.decodeButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(537, 307);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(262, 101);
            this.panel2.TabIndex = 9;
            // 
            // copyCipher
            // 
            this.copyCipher.BackColor = System.Drawing.Color.Black;
            this.copyCipher.Cursor = System.Windows.Forms.Cursors.Hand;
            this.copyCipher.Dock = System.Windows.Forms.DockStyle.Top;
            this.copyCipher.ForeColor = System.Drawing.Color.Lime;
            this.copyCipher.Location = new System.Drawing.Point(0, 0);
            this.copyCipher.Name = "copyCipher";
            this.copyCipher.Size = new System.Drawing.Size(262, 43);
            this.copyCipher.TabIndex = 8;
            this.copyCipher.Text = "Copy text to clipboard";
            this.copyCipher.UseVisualStyleBackColor = false;
            this.copyCipher.Click += new System.EventHandler(this.copyCipher_Click);
            // 
            // decodeButton
            // 
            this.decodeButton.BackColor = System.Drawing.Color.Black;
            this.decodeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.decodeButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.decodeButton.ForeColor = System.Drawing.Color.Lime;
            this.decodeButton.Location = new System.Drawing.Point(0, 53);
            this.decodeButton.Name = "decodeButton";
            this.decodeButton.Size = new System.Drawing.Size(262, 48);
            this.decodeButton.TabIndex = 7;
            this.decodeButton.Text = "Encode";
            this.decodeButton.UseVisualStyleBackColor = false;
            this.decodeButton.Click += new System.EventHandler(this.decodeButton_Click);
            // 
            // OneTimePad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(802, 411);
            this.Controls.Add(this.MainLayout);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OneTimePad";
            this.Text = "One Time Pad";
            this.Load += new System.EventHandler(this.OneTimePad_Load);
            this.MainLayout.ResumeLayout(false);
            this.MainLayout.PerformLayout();
            this.generatorPanel.ResumeLayout(false);
            this.generatorPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel MainLayout;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label keyLabel;
        private System.Windows.Forms.Label messageLabel;
        private System.Windows.Forms.TextBox outputTextBox;
        private System.Windows.Forms.TextBox keyTextBox;
        private System.Windows.Forms.TextBox messageTextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton decoideRadio;
        private System.Windows.Forms.RadioButton encodeRadio;
        private System.Windows.Forms.Button copyMessage;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button copyCipher;
        private System.Windows.Forms.Button decodeButton;
        private System.Windows.Forms.Panel generatorPanel;
        private System.Windows.Forms.Button copyKey;
        private System.Windows.Forms.Button genRandom;
        private System.Windows.Forms.MaskedTextBox genTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
    }
}
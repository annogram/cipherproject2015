﻿using CipherProject.CipherEncoder;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CipherProject.Frames {
    public partial class OneTimePad : Form {
        OneTimePadCipher cipher;
        bool encode = true;
        public OneTimePad() {
            InitializeComponent();
            encodeRadio.Checked = true;
            cipher = new OneTimePadCipher();
        }


        private void genRandom_Click(object sender, EventArgs e) {
            if (!genTextBox.Text.Equals(String.Empty)) {
                int length = int.Parse(genTextBox.Text);
                string key = cipher.GenerateRandomKey(length);
                keyTextBox.Text = key;
            }
        }

        private void OneTimePad_Load(object sender, EventArgs e) {

        }

        private void encodeRadio_CheckedChanged(object sender, EventArgs e) {
            encode = true;
            decodeButton.Text = "Encode";
        }

        private void decoideRadio_CheckedChanged(object sender, EventArgs e) {
            encode = false;
            decodeButton.Text = "Decode";
        }

        private void decodeButton_Click(object sender, EventArgs e) {
            try {
                if (encode) {
                    cipher.Key = keyTextBox.Text;
                    outputTextBox.Text = cipher.Encode(messageTextBox.Text);
                } else {
                    cipher.Key = keyTextBox.Text;
                    outputTextBox.Text = cipher.Decode(messageTextBox.Text);
                }
            } catch (FormatException ex) {
                DialogResult confirmError = MessageBox.Show(this, ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void copyMessage_Click(object sender, EventArgs e) {
            Clipboard.SetText(messageTextBox.Text);
        }

        private void copyKey_Click(object sender, EventArgs e) {
            Clipboard.SetText(keyTextBox.Text);
        }

        private void copyCipher_Click(object sender, EventArgs e) {
            Clipboard.SetText(outputTextBox.Text);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CipherProject.CipherEncoder;
using System.Windows.Forms;
using CipherProject.Util;
using System.IO;
using CipherProject.Information;
using CipherProject.Frames;
using System.Media;

namespace CipherProject {
    public partial class Cipher : Form {
        const string defaultAlpha = Constants.DEFAULT_ALPHA;
        private List<ICipher> _selectCipher = new List<ICipher>();
        private CaeserCipher caeser = new CaeserCipher(3);
        private NumerologicalCaeserCipher numCaeser = new NumerologicalCaeserCipher(3);
        private AtbashCipher atbash = new AtbashCipher();
        private VigenereCipher vigenere = new VigenereCipher("DEFAULT");
        private MorseCipher morse = new MorseCipher();
        CipherInfo info = new CipherInfo();
        AbortableBackgroundWorker morsePlayWorker = new AbortableBackgroundWorker();

        public Cipher() {
            InitializeComponent();
            radioButton1.Checked = true;
            // add the Encoders to a data list
            _selectCipher.Add(caeser);
            _selectCipher.Add(numCaeser);
            _selectCipher.Add(morse);
            _selectCipher.Add(atbash);
            _selectCipher.Add(vigenere);
            morsePlayWorker.DoWork += morsePlayWorker_DoWork;
            morsePlayWorker.RunWorkerCompleted += MorsePlayWorker_RunWorkerCompleted;
            SelectCipher.DataSource = _selectCipher;
        }

        private void MorsePlayWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            playMorseCode.Text = "Play";
        }

        private void Cipher_Load(object sender, EventArgs e) {
            infoLabel.Text = info.GetInfo(CipherInfoData.Initial);
        }

        private void openTextFileToolStripMenuItem_Click(object sender, EventArgs e) {
            if (openTextFile.ShowDialog() == DialogResult.OK) {
                string text = File.ReadAllText(openTextFile.FileName);
                InputArea.Text = text;
                infoLabel.Text = info.GetInfo(CipherInfoData.OpenTextFile);
            }
        }

        //Calls the background worker below then updates the label
        private void textAreaContent_TextChange(object sender, EventArgs e) {

        }

        //Background thread for cypher, this should call the cipher selected.
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e) {
            ICipher selCipher = e.Argument as AbstractCipher;

            string code;
            try {
                if (radioButton1.Checked) {
                    code = selCipher.Encode(InputArea.Text);
                    e.Result = code;
                } else if (radioButton2.Checked) {
                    code = selCipher.Decode(InputArea.Text);
                    e.Result = code;
                }
            } catch (NullReferenceException) {
                e.Result = "Please type in a keyword";
            }
        }

        //updates the text field when the cryptographic lookup is done.
        private void cryptographicWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            CryptographicText.Text = e.Result as string;
            infoLabel.Text = info.GetInfo(CipherInfoData.Done);
        }
        private void radioButton1_CheckedChanged_1(object sender, EventArgs e) {
            button1.Text = "Encode";
            TextEditorHeader.Text = "Text to be encoded";
            button1.BackColor = Color.Maroon;
        }

        private void radioButton2_CheckedChanged_1(object sender, EventArgs e) {
            button1.Text = "Decode";
            TextEditorHeader.Text = "Text to be decoded";
            button1.BackColor = Color.Aqua;
        }

        // onclick begin the costly operation
        private void button1_Click(object sender, EventArgs e) {
            ICipher toCheck = SelectCipher.SelectedItem as AbstractCipher;
            InputArea.Text = toCheck.checkFormat(InputArea.Text);
                while (cryptWorker.IsBusy) {
                    infoLabel.Text = info.GetInfo(CipherInfoData.Working);
                }
                cryptWorker.RunWorkerAsync(toCheck);
            //update information label
            if (!InputArea.Text.Equals("")) {
                infoLabel.Text = info.GetInfo(CipherInfoData.Working);
            }
        }

        //clear the text fields
        private void ClearText_Click(object sender, EventArgs e) {
            InputArea.Text = string.Empty;
            CryptographicText.Text = string.Empty;
            infoLabel.Text = info.GetInfo(CipherInfoData.Clear);
        }

        // show components depending on selected cipher
        private void SelectCipher_SelectedIndexChanged(object sender, EventArgs e) {
            ICipher selected = SelectCipher.SelectedItem as AbstractCipher;
            if (selected.ToString().Contains("Caeser")) {
                shiftLabel.Visible = true;
                shiftMaskedTextBox.Visible = true;
                setShiftButton.Visible = true;
                keywordLabel.Visible = false;
                keyWordText.Visible = false;
                keywordSet.Visible = false;
                playMorseCode.Visible = false;
            } else if (selected.ToString().Contains("Vigenere")) {
                keywordLabel.Visible = true;
                keyWordText.Visible = true;
                keywordSet.Visible = true;
                shiftLabel.Visible = false;
                shiftMaskedTextBox.Visible = false;
                setShiftButton.Visible = false;
                playMorseCode.Visible = false;
            }else if (selected.ToString().Contains("Morse")) {
                shiftLabel.Visible = false;
                shiftMaskedTextBox.Visible = false;
                setShiftButton.Visible = false;
                keywordLabel.Visible = false;
                keyWordText.Visible = false;
                keywordSet.Visible = false;
                playMorseCode.Visible = true;
            } else {
                shiftLabel.Visible = false;
                shiftMaskedTextBox.Visible = false;
                setShiftButton.Visible = false;
                keywordLabel.Visible = false;
                keyWordText.Visible = false;
                keywordSet.Visible = false;
                playMorseCode.Visible = false;
            }
            //Update information
            infoLabel.Text = info.GetInfo(selected.ToString());
        }

        private void setShiftButton_Click(object sender, EventArgs e) {
            CaeserCipher shifter = SelectCipher.SelectedItem as CaeserCipher;
            try {
                shifter.shiftAlphabet(int.Parse(shiftMaskedTextBox.Text));
            }catch (FormatException) {
                SystemSounds.Beep.Play();
            }
            infoLabel.Text = info.GetInfo(CipherInfoData.ShiftSet);
        }

        private void keywordSet_Click(object sender, EventArgs e) {
            vigenere.UpdateKeyword(keyWordText.Text);
            infoLabel.Text = info.GetInfo(CipherInfoData.KeyWordSet);
        }



        private void openFileDialog1_FileOk(object sender, CancelEventArgs e) {
        }

        private void saveTextFileToolStripMenuItem_Click(object sender, EventArgs e) {
            if (saveTextFile.ShowDialog() == DialogResult.OK) {
                System.IO.File.WriteAllText(saveTextFile.FileName, CryptographicText.Text);
                infoLabel.Text = info.GetInfo(CipherInfoData.SaveTextFile);
            }
        }

        private void clipboard_Click_1(object sender, EventArgs e) {
            if (CryptographicText.Text != null) {
                try {
                    Clipboard.SetText(CryptographicText.Text);
                    infoLabel.Text = info.GetInfo(CipherInfoData.Clipboard);
                } catch (ArgumentNullException) {
                    DialogResult confirmError = MessageBox.Show(this, "Nothing to copy", "Please encrypt a message to copy", MessageBoxButtons.OK);
                }
            }
        }

        private void InputArea_KeyPress(object sender, KeyPressEventArgs e) {
            if (e.KeyChar == (char)13) {
                ICipher toCheck = SelectCipher.SelectedItem as ICipher;
                InputArea.Text = toCheck.checkFormat(InputArea.Text);
                cryptWorker.RunWorkerAsync(toCheck);
            }
        }

      

        private void createNewToolStripMenuItem_Click(object sender, EventArgs e) {
            var oneTimePad1 = new OneTimePad();
            oneTimePad1.Show();
        }

        private void button2_Click(object sender, EventArgs e) {
            if (playMorseCode.Text.Equals("Stop")) {
                morsePlayWorker.Abort();
                playMorseCode.Text = "Play";
            } else {
                try {
                    morsePlayWorker.RunWorkerAsync();
                    playMorseCode.Text = "Stop";
                } catch (FormatException) {
                    infoLabel.Text = info.GetInfo(CipherInfoData.BadFormat);
                }
            }
        }
        
        private void morsePlayWorker_DoWork(object sender, DoWorkEventArgs e) {
            morse.playMessage(CryptographicText.Text);
        }

        private void morsePlayWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {

        }
    }
}

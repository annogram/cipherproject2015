﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CipherProject.CipherEncoder {
    /// <summary>
    /// The Caeser cipher uses a simple alphabetic shift to encode the string.
    /// </summary>
    public class CaeserCipher : AbstractCipher {
        protected Dictionary<char, char> LanguageEncode = new Dictionary<char, char>();
        protected Dictionary<char, char> LanguageDecode = new Dictionary<char, char>();
        private int shift;
        /// <summary>
        /// The Caeser cipher uses a simple alphabetic shift left to encode the alphabet.
        /// </summary>
        /// <param name="shift">The number of places the alphabet will shift.</param>
        public CaeserCipher(int shift) {
            char[] cycle = DEFAULT_ALPHA.ToCharArray();
            int select;
            this.shift = shift;

            // Sequence the alphabet after the shift length
            for (int i = 0; i < cycle.Length; i++) {
                select = (shift > 0) ? (i + shift) % cycle.Length : (i + shift + cycle.Length) % cycle.Length;
                LanguageEncode.Add(cycle[i], cycle[select]);
                LanguageDecode.Add(cycle[select], cycle[i]);
            }
        }

         public override char CryptographicLanguage(char input, bool reverse) {
            if (reverse) {
                char output;
                LanguageDecode.TryGetValue(input, out output);
                return output;
            } else {
                char output;
                LanguageEncode.TryGetValue(input, out output);
                return output;
            }
        }

        public override string ToString() {
            return "Caeser";
        }

       
        public virtual void shiftAlphabet(int shiftAmount) {
            char[] cycle = DEFAULT_ALPHA.ToCharArray();
            int select;
            LanguageDecode = new Dictionary<char, char>();
            LanguageEncode = new Dictionary<char, char>();
            for (int i = 0; i < cycle.Length; i++) {
                select = (shiftAmount > 0) ? (i + shiftAmount) % cycle.Length : (i + shiftAmount + cycle.Length) % cycle.Length;
                LanguageEncode.Add(cycle[i], cycle[select]);
                LanguageDecode.Add(cycle[select], cycle[i]);
            }
        }

       
    }
}

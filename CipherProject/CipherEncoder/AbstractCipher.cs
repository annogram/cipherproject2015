﻿using CipherProject.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CipherProject.CipherEncoder {
    public abstract class AbstractCipher : ICipher {

        protected const string DEFAULT_ALPHA = Constants.DEFAULT_ALPHA;
        /// <summary>
        /// This abstract class needs to implement a the cryptographic function and map the language 
        /// in some way to return every alpha numeric character and punctuation.
        /// </summary>
        /// <param name="input">The character to be encoded or decoded</param>
        /// <param name="reverse">When this is set to true the function should do a decode, when this is false it should encode</param>
        /// <returns></returns>
        public abstract char CryptographicLanguage(char input, bool reverse);

        /// <summary>
        /// This abstract method is required to be overriden so that the value can 
        /// appear human readable on the GUI list.
        /// </summary>
        /// <returns>The name of the cipher</returns>
        public abstract override string ToString();

        /// <summary>
        /// Decodes an encrypted string.
        /// </summary>
        /// <param name="input">Text to be decrypted.</param>
        /// <returns></returns>
        public virtual string Decode(string input) {
            char[] characters = input.ToCharArray();
            characters = characters.Select(x => CryptographicLanguage(x, true)).ToArray();
            return string.Concat(characters.AsEnumerable());
        }

        /// <summary>
        /// Encodes an unencrypted string.
        /// </summary>
        /// <param name="input">Text to be encrypted.</param>
        /// <returns></returns>
        public virtual string Encode(string input) {
            char[] characters = input.ToCharArray();
            characters = characters.Select(x => CryptographicLanguage(x, false)).ToArray();
            return string.Concat(characters.AsEnumerable());
        }

        /// <summary>
        /// This is needed to clear the text field of incompatible characters before transfer.
        /// If this is not overriden then the default method will be used.
        /// </summary>
        /// <param name="textToCheck">Input to be parsed and cleared of invalid characters</param>
        /// <returns>The string cleared of invalid characters.</returns>
        public virtual string checkFormat(string textToCheck) {
            char[] check = textToCheck.Select(s => (char)s).Where(s => DEFAULT_ALPHA.Contains(s)).ToArray();
            return string.Concat(check);
        }
    }
}

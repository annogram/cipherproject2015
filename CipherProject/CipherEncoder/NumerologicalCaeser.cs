﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CipherProject.CipherEncoder {
    /// <summary>
    /// This encryption is a combination of the classic Caeser cipher and the
    /// A1Z26 cipher, which uses numeric representation of characters on a shifted
    /// alphabet. 
    /// </summary>
    public class NumerologicalCaeserCipher : CaeserCipher {
        private char[] _langBuffer;
        new protected Dictionary<string, int> LanguageEncode = new Dictionary<string, int>();
        new protected Dictionary<int, char> LanguageDecode = new Dictionary<int, char>();
        /// <summary>
        /// This encryption is a combination of the classic Caeser cipher and the
        /// A1Z26 cipher, which uses numeric representation of characters on a shifted
        /// alphabet.
        /// </summary>
        public NumerologicalCaeserCipher(int shift) : base(shift) {
            _langBuffer = DEFAULT_ALPHA.Select(c => base.CryptographicLanguage(c, false)).ToArray();
            for (int i = 0; i < _langBuffer.Length; i++) {
                LanguageEncode.Add(_langBuffer[i] + "", i);
                LanguageDecode.Add(i, _langBuffer[i]);
            }
        }

        public override char CryptographicLanguage(char input, bool reverse) {
            if (reverse) {
                char output;
                LanguageDecode.TryGetValue(input, out output);
                return output;
            } else {
                int output;
                LanguageEncode.TryGetValue(input + "", out output);
                string morph = "" + output;
                return char.Parse(morph);
            }
        }

        private string CryptographicLanguage(string input, bool reverse) {
            if (reverse) {
                char output;
                    LanguageDecode.TryGetValue(int.Parse(input), out output);
                return "" + output;
            } else {
                int output;
                LanguageEncode.TryGetValue(input, out output);
                string morph = "" + output;
                return morph;
            }
        }

        // We must overide the encode and decode methods for this class since the return format
        // is non standard
        public override string Encode(string input) {
            StringBuilder sb = new StringBuilder();
            foreach (char c in input) {
                sb.Append(this.CryptographicLanguage(c + "", false) + "-");
            }
            return sb.ToString().TrimEnd('-');
        }

        public override string Decode(string input) {
            string[] morphInput = input.Split('-');
            string[] output;
            try {
                output = morphInput.Select(s => this.CryptographicLanguage(s, true)).ToArray();
            } catch (FormatException) {
                return "This text is not in a format that can be deciphered: Correct format is: \"34-5-7-1-49-16-4-5-15\"";
            }
            return string.Concat(output);
        }

        public override string ToString() {
            return "A1Z26 Caeser";
        }

        public override void shiftAlphabet(int shiftAmount) {
            base.shiftAlphabet(shiftAmount);
            LanguageEncode = new Dictionary<string, int>();
            LanguageDecode = new Dictionary<int, char>();
            _langBuffer = DEFAULT_ALPHA.Select(c => base.CryptographicLanguage(c, false)).ToArray();
            for (int i = 0; i < _langBuffer.Length; i++) {
                LanguageEncode.Add(_langBuffer[i] + "", i);
                LanguageDecode.Add(i, _langBuffer[i]);
            }
        }
    }
}

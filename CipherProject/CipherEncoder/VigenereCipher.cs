﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CipherProject.CipherEncoder {
    /// <summary>
    /// This cipher uses an 2D map of Caeser ciphers as an alphabet and a key to encrypt
    /// a word.
    /// </summary>
    public class VigenereCipher : AbstractCipher {
        private CaeserCipher[] VigenereGrid;
        private int position = 0;
        public VigenereCipher(string keyword) {
            char[] defaultAlpha = DEFAULT_ALPHA.ToCharArray();
            char[] keywordSplit = keyword.ToCharArray();
            VigenereGrid = new CaeserCipher[keywordSplit.Length];
            for (int i = 0; i < keyword.Length; i++) {
                char test = keywordSplit[i];
                for (int j = 0; j < DEFAULT_ALPHA.Length; j++) {
                    if (test == defaultAlpha[j]) {
                        VigenereGrid[i] = new CaeserCipher(j);
                    }
                }
            }
        }

        public override char CryptographicLanguage(char input, bool reverse) {
            string output = (reverse) ? VigenereGrid[position].Decode(input + "") : VigenereGrid[position].Encode(input + "");
            position = ++position % VigenereGrid.Length;
            return char.Parse(output);
        }

        public override string ToString() {
            return "Vigenere";
        }

        public override string Encode(string input) {
            string output = base.Encode(input);
            position = 0;
            return output;
        }

        public override string Decode(string input) {
            string output = base.Decode(input);
            position = 0;
            return output;
        }

        public void UpdateKeyword(string keyword) {
            char[] defaultAlpha = DEFAULT_ALPHA.ToCharArray();
            char[] keywordSplit = keyword.ToCharArray();
            VigenereGrid = new CaeserCipher[keywordSplit.Length];
            for (int i = 0; i < keyword.Length; i++) {
                char test = keywordSplit[i];
                for (int j = 0; j < DEFAULT_ALPHA.Length; j++) {
                    if (test == defaultAlpha[j]) {
                        VigenereGrid[i] = new CaeserCipher(j);
                    }
                }
            }
        }

    }
}

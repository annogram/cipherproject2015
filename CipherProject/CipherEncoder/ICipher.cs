﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CipherProject.CipherEncoder {
    interface ICipher {
        char CryptographicLanguage(char input, bool reverse );
        string checkFormat(string textToCheck);
        string Decode(string input);
        string Encode(string input);
    }
}

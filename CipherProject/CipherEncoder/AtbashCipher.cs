﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CipherProject.CipherEncoder {
    /// <summary>
    /// This cipher simply reverses the alphabet
    /// </summary>
    class AtbashCipher : AbstractCipher {

        public override char CryptographicLanguage(char input, bool reverse) {
            char[] reversedAlpha = DEFAULT_ALPHA.Reverse().ToArray();
            if (reverse) { 
                for(int i = 0; i < DEFAULT_ALPHA.Length; i++) {
                    if (input == reversedAlpha[i]) return DEFAULT_ALPHA[i];
                }
                return '\0';
            } else {
                for (int i = 0; i < DEFAULT_ALPHA.Length; i++) {
                    if (input == DEFAULT_ALPHA[i]) return reversedAlpha[i];
                }
                return '\0';
            }
        } 

        public override string ToString() {
            return "Atbash";
        }
    }
}

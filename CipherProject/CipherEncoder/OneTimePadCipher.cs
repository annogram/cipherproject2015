﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CipherProject.Util;
using System.Threading.Tasks;

namespace CipherProject.CipherEncoder {
        /// <summary>
        /// A one time pad is a method of encryption which is theoretically 
        /// impossible to crack. The behaviour is very much like the Vigenere except
        /// that the key is not a repeating string but a possibly random string the length 
        /// of the message.
        /// </summary>
    public class OneTimePadCipher { 
        public string Key { get; set; }
        private const string ALPHABET = Util.Constants.DEFAULT_ALPHA;

        /// <summary>
        /// This encode function will iterate through every character in the message
        /// and add the value of the key at that location to the value of the message.
        /// Character value is determinded from the default alphabet.
        /// </summary>
        /// <param name="message">The users message to be encrypted</param>
        /// <returns>An encrypted message as a string</returns>
        public string Encode(string message) {
            StringBuilder sb = new StringBuilder();
            if (message.Length < Key.Length) {
                for(int i = 0; i < message.Length; i++) {
                    int messageCharValue = ALPHABET.IndexOf(message[i]);
                    int keyCharValue = ALPHABET.IndexOf(Key[i]);
                    int finalValue = (messageCharValue + keyCharValue) % ALPHABET.Length;
                    sb.Append(ALPHABET[finalValue]);
                }
            } else {
                throw new FormatException("Message length greater than key length");
            }
            return sb.ToString();
        }

        public string Decode(string encryptedMessage) {
            StringBuilder sb = new StringBuilder();
            if (encryptedMessage.Length < Key.Length) {
                for(int i = 0; i < encryptedMessage.Length; i++) {
                    int messageCharValue = ALPHABET.IndexOf(encryptedMessage[i]);
                    int keyCharValue = ALPHABET.IndexOf(Key[i]);
                    int finalValue = (messageCharValue - keyCharValue).Mod(ALPHABET.Length);
                    sb.Append(ALPHABET[finalValue]);
                }
            } else {
                throw new FormatException("Message length greater than key length");
            }
            return sb.ToString();
        }

        public string GenerateRandomKey(int length) {
            StringBuilder sb = new StringBuilder();
            Random rand = new Random();
            int loc;
            for (int i = 0;  i < length; i++) {
                loc = rand.Next(ALPHABET.Length);
                sb.Append(ALPHABET[loc]);
            }
            return sb.ToString();
        }

       
        public override string ToString() {
            return "One time pad";
        }
    }
}

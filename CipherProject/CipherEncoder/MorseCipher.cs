﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CipherProject.CipherEncoder {
    class MorseCipher : AbstractCipher {
        Dictionary<char, string> LanguageEncoder = new Dictionary<char, string>();
        Dictionary<string, char> LanguageDecoder = new Dictionary<string, char>();
        string alphabet = "abcdefghijklmnopqrstuvwxyz1234567890 ";
        string[] morseAlphabet = new string[] { ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....",
            "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-","..-","...-",
            ".--", "-..-", "-.--", "--..", ".----","..---", "...--", "....-", ".....", "-....", "--...", "---..", "----.", "-----",
         "\t"};
        public MorseCipher() {
            for (int i = 0; i < alphabet.Length; i++) {
                LanguageEncoder.Add(alphabet[i], morseAlphabet[i]);
                LanguageDecoder.Add(morseAlphabet[i], alphabet[i]);
            }
        }

        // unused
        public override char CryptographicLanguage(char input, bool reverse) {
            return '\0';
        }

        public string CrypographicLanguage(string input, bool reverse) {
            if (reverse) {
                char output;
                LanguageDecoder.TryGetValue(input, out output);
                return output + "";
            } else {
                string output;
                LanguageEncoder.TryGetValue(char.Parse(input), out output);
                return output + " ";
            }
        }

        public override string Encode(string input) {
            string[] outArray = input.Select(s => this.CrypographicLanguage(s + "", false)).ToArray();
            string toTrim = string.Concat(outArray);
            return toTrim.TrimEnd(' ');
        }

        public override string Decode(string input) {
            string[] toParse = input.Split(' ');
            string[] outArray = toParse.Select(s => this.CrypographicLanguage(s + "", true)).ToArray();
            return string.Concat(outArray);
        }

        public override string ToString() {
            return "Morse code";
        }

        public override string checkFormat(string textToCheck) {
            char[] check = textToCheck.Select(s => char.ToLower(s, System.Globalization.CultureInfo.CurrentCulture)).Where(s => alphabet.Contains(s)).ToArray();
            if (textToCheck.Contains("-")) {
                return textToCheck;
            }
            return string.Concat(check);
        }

        /// <summary>
        /// Plays the message using system beeps
        /// </summary>
        /// <param name="message">String in morse code to be played</param>
        public void playMessage(string message) {
            if (message.Contains(DEFAULT_ALPHA)) {
                throw new FormatException();
            }
            string[] playList = message.Split(' ');
            int frequency = 1000;
            int dot = 100;
            foreach (char c in message) {
                switch (c) {
                    case '-':
                        Console.Beep(frequency,dot * 3);
                        break;
                    case '.':
                        Console.Beep(frequency, dot);
                        break;
                    case ' ':
                        System.Threading.Thread.Sleep(dot);
                        break;
                }
                System.Threading.Thread.Sleep(dot);
            }

        }
    }
}

﻿using System;
using CipherProject.CipherEncoder;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CipherTest {
    [TestClass]
    public class OneTimePadTests {
        [TestMethod]
        public void UnitTest_OneTimePad_TestEncodeMethod() {
            //arrange
            OneTimePadCipher cipher = new OneTimePadCipher();
            cipher.Key = "If only i knew how";
            string messageIn = "My ass hurts";
            string expectedOut = "eDKoFDi6CbDF";
            //act
            string actualOut = cipher.Encode(messageIn);
            //assert
            Assert.AreEqual(expectedOut, actualOut, "Expected: " + expectedOut);
        }

        [TestMethod]
        public void UnitTest_OneTimePad_TestDecodeMethod() {
            // arrange
            OneTimePadCipher cipher = new OneTimePadCipher();
            cipher.Key = "If only i knew how";
            string messageIn = "eDKoFDi6CbDF";
            string expectedOut = "My ass hurts";
            // act
            string actualOut = cipher.Decode(messageIn);
            // assert
            Assert.AreEqual(expectedOut, actualOut);
        }

        [TestMethod]
        public void UnitTest_OneTimePad_TestRandomNumberGeneration() {
            // arrange
            OneTimePadCipher cipher = new OneTimePadCipher();
            int length = 216;
            // act
            string output = cipher.GenerateRandomKey(length);
            // assert
            Assert.AreEqual(length, output.Length);
        }
    }
}

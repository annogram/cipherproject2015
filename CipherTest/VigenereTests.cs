﻿using System;
using CipherProject.CipherEncoder;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CipherTest {
    [TestClass]
    public class VigenereTests {
        VigenereCipher cipher;
        [TestInitialize]
        public void Initialize() {
            cipher = new VigenereCipher("GrantHawthorne");
        }

        [TestMethod]
        public void UnitTest_VigenereCipher_EncodeKeywordSameDigit() {
            // arrange
            string expectedOut = "XIrEKYrNKyF";
            string input = "rrrrrrrrrrr";
            string actualOut;
            // act
            actualOut = cipher.Encode(input);
            // assert
            Assert.AreEqual(expectedOut, actualOut, "Expected '" + expectedOut + "' but got: " + actualOut);
        }
    }
}

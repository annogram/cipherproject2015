﻿using System;
using CipherProject.CipherEncoder;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CipherTest {
    [TestClass]
    public class CaeserTests {
        [TestMethod]
        public void UnitTest_CaeserCipher_ConstructorCreatesDictionary() {
            //arrange
            CaeserCipher test = new CaeserCipher(3);
            //abc = def
            string expectedOut = "def";
            string actualOut;
            //act
            actualOut = test.Encode("abc");
            //assert
            Assert.AreEqual(expectedOut, actualOut, "Expected 'def' but got: " + actualOut);
        }

        [TestMethod]
        public void UnitTest_CaeserCipher_CheckNegativeShift() {
            //arrange
            CaeserCipher test = new CaeserCipher(-3);
            string expectedOut = "abc";
            string actualOut;
            //act
            actualOut = test.Encode("def");
            //assert
            Assert.AreEqual(expectedOut, actualOut, "Expected 'def' but got: " + actualOut);
        }

        [TestMethod]
        public void UnitTest_CaeserCipher_DecodeTest() {
            //arrange
            CaeserCipher test = new CaeserCipher(3);
            string expectedOut = "jkl";
            string actualOut;
            //act
            actualOut = test.Decode("mno");
            //assert
            Assert.AreEqual(expectedOut, actualOut, "Expected 'jkl' but got: " + actualOut);
        }
        [TestMethod]
        public void UnitTest_CaeserCipher_DecodeTestNegative() {
            //arrange
            CaeserCipher test = new CaeserCipher(-3);
            string expectedOut = "pqr";
            string actualOut;
            //act
            actualOut = test.Decode("mno");
            //assert
            Assert.AreEqual(expectedOut, actualOut, "Expected 'pqr' but got: " + actualOut);
        }
    }
}

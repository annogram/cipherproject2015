﻿using System;
using CipherProject.CipherEncoder;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CipherTest {
    [TestClass]
    public class NumerologicalCaeserTests{
        AbstractCipher cipher;
        [TestInitialize]
        public void InitializeTest() {
            cipher = new NumerologicalCaeserCipher(3);
        } 

        [TestMethod]
        public void UnitTest_NumerologicalCaeserCipher_EncodeWithPositiveShift() {
            //arrange
            string expectedOut = "20-21-22";
            string input = "xyz";
            string actualOut;
            //act
            actualOut = cipher.Encode(input);
            //assert
            Assert.AreEqual(expectedOut, actualOut, "Expected '1-2-3', but got: " + actualOut);
        }

        [TestMethod]
        public void UnitTest_NumerologicalCaeserCipher_DecodeWithPositiveShift() {
            //arrange
            string expectedOut = "xyz";
            string input = "20-21-22";
            string actualOut;
            //act
            actualOut = cipher.Decode(input);
            //assert
            Assert.AreEqual(expectedOut, actualOut, "Expected 'xyz', but got: " + actualOut);
        }
    }
}
